<?php

namespace Feature\Domain;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\TableNode;
use Broadway\Auditing\CommandLogger;
use Broadway\Auditing\CommandSerializer;
use Broadway\CommandHandling\EventDispatchingCommandBus;
use Broadway\CommandHandling\SimpleCommandBus;
use Broadway\EventDispatcher\EventDispatcher;
use Psr\Log\LogLevel;
use RuntimeException;
use StdoutLogger;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\StreamOutput;
use Tworzenieweb\SoloPlanner\Application\Command\CreateActivityCommand;
use Tworzenieweb\SoloPlanner\Application\Command\DeleteActivityCommand;
use Tworzenieweb\SoloPlanner\Application\Command\EditActivityCommand;
use Tworzenieweb\SoloPlanner\Application\Command\ViewActivitiesCommand;
use Tworzenieweb\SoloPlanner\Application\Handler\CreateActivityHandler;
use Tworzenieweb\SoloPlanner\Application\Handler\DeleteActivityHandler;
use Tworzenieweb\SoloPlanner\Application\Handler\EditActivityHandler;
use Tworzenieweb\SoloPlanner\Application\Handler\ViewActivitiesHandler;
use Tworzenieweb\SoloPlanner\Application\ViewDTO;
use Tworzenieweb\SoloPlanner\Domain\Category;
use Tworzenieweb\SoloPlanner\Domain\Category\CategoryId;
use Tworzenieweb\SoloPlanner\Domain\CategoryRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date\InvalidArgumentException as DateInvalidArgumentException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time\InvalidArgumentException;
use Tworzenieweb\SoloPlanner\Domain\TimeplanRepository;
use Tworzenieweb\SoloPlanner\Domain\User;
use Tworzenieweb\SoloPlanner\Domain\UserRepository;
use Tworzenieweb\SoloPlanner\Infrastructure\Activity\InMemoryActivityRepository;
use Tworzenieweb\SoloPlanner\Infrastructure\Activity\InMemoryCreatedOnDateActivitySpecification;
use Tworzenieweb\SoloPlanner\Infrastructure\Category\InMemoryCategoryRepository;
use Tworzenieweb\SoloPlanner\Infrastructure\Category\InMemoryFilterByIdsCategorySpecification;
use Tworzenieweb\SoloPlanner\Infrastructure\Timeplan\InMemoryTimeplanRepository;
use Tworzenieweb\SoloPlanner\Infrastructure\User\InMemoryUserRepository;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 */
class ActivityContext implements Context, SnippetAcceptingContext
{
    /**
     * @var SimpleCommandBus
     */
    private $commandBus;

    /**
     * @var CreateActivityCommand
     */
    private $createActivityCommand;

    /**
     * @var ViewActivitiesCommand
     */
    private $viewActivitiesCommand;

    /**
     * @var User
     */
    private $user;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var TimeplanRepository
     */
    private $timeplanRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var array
     */
    private $nameToCategoryId;

    /**
     * @var ViewDTO
     */
    private $view;

    /**
     */
    public function __construct()
    {
        $this->categoryRepository = new InMemoryCategoryRepository();
        $this->activityRepository = new InMemoryActivityRepository();
        $this->timeplanRepository = new InMemoryTimeplanRepository();
        $this->userRepository = new InMemoryUserRepository();
        $this->user = new User($this->userRepository->nextIdentity());
        $this->userRepository->add($this->user);

        $commandHandler = new CreateActivityHandler(
            $this->timeplanRepository,
            $this->activityRepository,
            $this->categoryRepository,
            new InMemoryFilterByIdsCategorySpecification()
        );

        $this->view = new ViewDTO();

        $viewActivitiesHandler = new ViewActivitiesHandler(
            $this->timeplanRepository,
            $this->activityRepository,
            new InMemoryCreatedOnDateActivitySpecification(),
            $this->view
        );

        $deleteActivityHandler = new DeleteActivityHandler(
            $this->activityRepository,
            $this->userRepository
        );

        $editActivityHandler = new EditActivityHandler(
            $this->activityRepository,
            $this->userRepository,
            $this->categoryRepository,
            new InMemoryFilterByIdsCategorySpecification()
        );

        $eventDispatcher  = new EventDispatcher();
        $this->commandBus = new SimpleCommandBus();
        $this->commandBus = new EventDispatchingCommandBus($this->commandBus, $eventDispatcher);

        $this->commandBus->subscribe($commandHandler);
        $this->commandBus->subscribe($viewActivitiesHandler);
        $this->commandBus->subscribe($deleteActivityHandler);
        $this->commandBus->subscribe($editActivityHandler);


        $this->createActivityCommand = new CreateActivityCommand();
        $this->viewActivitiesCommand = new ViewActivitiesCommand();

        $verbosityLevelMap = array(
            LogLevel::NOTICE => OutputInterface::VERBOSITY_NORMAL,
            LogLevel::INFO   => OutputInterface::VERBOSITY_NORMAL,
        );
        $logger            = new ConsoleLogger(new StreamOutput(fopen('php://stdout', 'w')), $verbosityLevelMap);
        $commandSerializer = new CommandSerializer();
        $commandAuditLogger = new CommandLogger($logger, $commandSerializer);

        $eventDispatcher->addListener('broadway.command_handling.command_success', array($commandAuditLogger, 'onCommandHandlingSuccess'));
        $eventDispatcher->addListener('broadway.command_handling.command_failure', array($commandAuditLogger, 'onCommandHandlingFailure'));
    }

    /**
     * @Given I have created a timeplan for user
     */
    public function iHaveCreatedATimeplanForUser()
    {
        $timeplan = $this->user->createTimeplan();
        $id = $timeplan->getId()->getId();
        $this->timeplanRepository->add($timeplan);
        $this->createActivityCommand->setTimeplanId($id);
        $this->viewActivitiesCommand->setTimeplanId($id);
    }


    /**
     * @Given there are the following activity categories available:
     */
    public function thereAreTheFollowingActivityCategoriesAvailable(TableNode $table)
    {
        $this->nameToCategoryId = [];
        foreach ($table as $row) {
            $categoryId = CategoryId::valueOf($row['id']);
            $this->nameToCategoryId[$row['name']] = $categoryId;
            $this->categoryRepository->add(new Category($categoryId, $row['name']));
        }
    }

    /**
     * @When I selected timeframe to be :date from :timeFrom to :timeTo
     */
    public function iSelectedTimeframeToBeFromTo($date, $timeFrom, $timeTo)
    {
        $this->createActivityCommand->setStartDate($date);
        $this->createActivityCommand->setStartTime($timeFrom);
        $this->createActivityCommand->setEndTime($timeTo);
    }


    /**
     * @When I assigned activity category to be :categoryName
     */
    public function iAssignedActivityCategoryToBe($categoryName)
    {
        $this->createActivityCommand->setCategoryIds([$this->nameToCategoryId[$categoryName]->getId()]);
    }

    /**
     * @When I assigned activity name to :name
     */
    public function iAssignedActivityNameTo($name)
    {
        $this->createActivityCommand->setName($name);
    }

    /**
     * @Then New activity should be created
     */
    public function newActivityShouldBeCreated()
    {
        $this->commandBus->dispatch($this->createActivityCommand);
    }

    /**
     * @Then I should be noticed about invalid time
     */
    public function thenIShouldBeNoticedAboutInvalidTime()
    {
        expect($this->commandBus)->shouldThrow(InvalidArgumentException::class)
            ->duringDispatch($this->createActivityCommand);
    }

    /**
     * @Then I should be noticed about invalid date
     */
    public function thenIShouldBeNoticedAboutInvalidDate()
    {
        expect($this->commandBus)->shouldThrow(DateInvalidArgumentException::class)
            ->duringDispatch($this->createActivityCommand);
    }

    /**
     * @Given I have the following activities in my repository:
     */
    public function iHaveTheFollowingActivitiesInMyRepository(TableNode $table)
    {
        $this->activityRepository->purge();

        foreach ($table as $node) {
            $this->createActivityCommand->setStartDate($node['startDate']);
            $this->createActivityCommand->setStartTime('08:00');
            $this->createActivityCommand->setEndTime('08:00');
            $this->createActivityCommand->setName($node['name']);
            $this->createActivityCommand->setCategoryIds([1]);

            $this->commandBus->dispatch($this->createActivityCommand);
        }
    }

    /**
     * @When I search for activities saved for :view :context
     */
    public function iSearchForActivitiesSavedFor($view, $context)
    {
        $this->viewActivitiesCommand->setView($view)
                                    ->setContext($context);

        $this->commandBus->dispatch($this->viewActivitiesCommand);
    }

    /**
     * @Then I should get :count with names :names
     */
    public function iShouldGetWithNames($count, $names)
    {
        expect($this->view->activities)->shouldHaveCount((int) $count);
        $this->resultsHaveNames($this->view->activities, $names);
    }

    /**
     * @When I remove :name
     */
    public function iRemove($name)
    {
        $activity = $this->activityRepository->ofName($name)[0];
        $deleteActivityCommand = new DeleteActivityCommand();
        $deleteActivityCommand->setId($activity->getId()->getId());
        $deleteActivityCommand->setUserId($this->user->getId()->getId());

        $this->commandBus->dispatch($deleteActivityCommand);

        expect($this->activityRepository)->ofId($activity->getId())->toBe(null);
    }

    /**
     * @param Activity[] $results
     * @param string     $names
     *
     * @throws RuntimeException
     */
    private function resultsHaveNames(array $results, $names)
    {
        foreach ($results as $result) {
            if (strstr($names, $result->getName()) === false) {
                throw new RuntimeException(sprintf('Element %s in not part of results %s', $result->getName(), $names));
            }
        }
    }


    /**
     * @When I make edit command on :name to have values:
     */
    public function iMakeEditCommandOnToHaveValues($name, TableNode $table)
    {

        $data = $table->getHash()[0];
        $categoryNames = explode(',', $data['categories']);
        $categoryIds = [];

        foreach ($categoryNames as $categoryName) {
            array_push($categoryIds, $this->nameToCategoryId[$categoryName]->getId());
        }

        $activity = $this->activityRepository->ofName($name)[0];
        $editCommand = new EditActivityCommand();
        $editCommand->setName($data['name']);
        $editCommand->setStartAtDate($data['startDate']);
        $editCommand->setStartTime($data['startTime']);
        $editCommand->setEndTime($data['endTime']);
        $editCommand->setCategoryIds($categoryIds);
        $editCommand->setUserId($this->user->getId()->getId());
        $editCommand->setActivityId($activity->getId()->getId());

        $this->commandBus->dispatch($editCommand);
    }

    /**
     * @Then I should get :count with attributes:
     */
    public function iShouldGetWithAttributes($count, TableNode $table)
    {
        expect($this->view->activities)->shouldHaveCount((int) $count);

        $activity = $this->view->activities[0];

        $row = $table->getHash()[0];
        expect($activity)->getName()->shouldBe($row['name']);
    }
}
