Feature: Activity management
  In order to manage my time and store my daily tasks
  I want to be able to add new activities
  And want to assign them into one of my predefined categories.
  Then activities should be available for browsing in four views: dayly, weekly, monthly and yearly

  Background:
    Given I have created a timeplan for user
    And there are the following activity categories available:
    | id | name     |
    | 1  | sen      |
    | 2  | rozrywka |
    | 3  | nauka    |

  Scenario: Attempt to create a valid activity
    When I selected timeframe to be "2016-01-01" from "12:00" to "14:00"
    And I assigned activity category to be "rozrywka"
    And I assigned activity name to "granie w x-boxa"
    Then New activity should be created

  Scenario: Attempt to create activity with invalid start time
    When I selected timeframe to be "2016-01-01" from "aaa" to "14:00"
    Then I should be noticed about invalid time

  Scenario: Attempt to create activity with invalid end time
    When I selected timeframe to be "2016-01-01" from "08:00" to "aa:00"
    Then I should be noticed about invalid time

  Scenario: Attempt to create activity with invalid date
    When I selected timeframe to be "aaaa" from "12:00" to "14:00"
    Then I should be noticed about invalid date

  Scenario Outline: Attempt to find activities for views
    Given I have the following activities in my repository:
      | name                 | startDate    |
      | granie w x-boxa      | 2016-01-01   |
      | czytanie             | 2016-01-06   |
      | jogging              | 2016-06-08   |
      | sen                  | 2016-01-11   |
      | impreza              | 2016-12-31   |
      | nauka                | 2015-12-31   |
    When I search for activities saved for <view> <context>
    Then I should get <count> with names <names>

    Examples:
      | view    | context      | count | names                                          |
      | "day"   | "2016-01-01" |   1   | "granie w x-boxa"                              |
      | "week"  | "2016/W01"   |   1   | "czytanie"                                     |
      | "month" | "01/2016"    |   3   | "granie w x-boxa,czytanie,sen"                 |
      | "year"  | "2016"       |   5   | "granie w x-boxa,czytanie,jogging,sen,impreza" |


    Scenario: Attempt to delete previously created activity
      Given I have the following activities in my repository:
        | name                 | startDate    |
        | granie w x-boxa      | 2016-01-01   |
        | czytanie             | 2016-01-06   |
      When I remove "czytanie"
      And I search for activities saved for "year" "2016"
      Then I should get "1" with names "granie w x-boxa"

    Scenario: Attempt to edit activity
      Given I have the following activities in my repository:
        | name                 | startDate    |
        | granie w x-boxa      | 2016-01-01   |
      When I make edit command on "granie w x-boxa" to have values:
        | name       |  startDate    |  startTime  | endTime | categories     |
        | czytanie   |  2016-01-02   |  16:00      |  20:00  | rozrywka,nauka |
      And I search for activities saved for "year" "2016"
      Then I should get "1" with attributes:
        | name       |  startDate    |  startTime  | endTime | categories     |
        | czytanie   |  2016-01-02   |  16:00      |  20:00  | rozrywka,nauka |


