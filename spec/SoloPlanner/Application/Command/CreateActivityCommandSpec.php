<?php

namespace spec\Tworzenieweb\SoloPlanner\Application\Command;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Application\Command
 */
class CreateActivityCommandSpec extends ObjectBehavior
{
    private $startDate = '2016-01-01';

    private $startTime = '08:00';

    private $endTime = '11:00';

    private $categories = [1, 2, 3];

    private $name = 'Some activity';

    private $note = 'Some note';

    private $timeplanId = 1;

    function let()
    {
        $this->setStartDate($this->startDate)->shouldReturn($this);
        $this->setStartTime($this->startTime)->shouldReturn($this);
        $this->setEndTime($this->endTime)->shouldReturn($this);
        $this->setName($this->name)->shouldReturn($this);
        $this->setNote($this->note)->shouldReturn($this);
        $this->setTimeplanId($this->timeplanId)->shouldReturn($this);
        $this->setCategoryIds($this->categories)->shouldReturn($this);
    }

    function it_assign_values()
    {
        $this->getStartDate()->shouldReturn($this->startDate);
        $this->getStartTime()->shouldReturn($this->startTime);
        $this->getEndTime()->shouldReturn($this->endTime);
        $this->getName()->shouldReturn($this->name);
        $this->getNote()->shouldReturn($this->note);
        $this->getTimeplanId()->shouldReturn($this->timeplanId);
        $this->getCategoryIds()->shouldReturn($this->categories);
    }
}
