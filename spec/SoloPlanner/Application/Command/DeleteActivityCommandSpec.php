<?php

namespace spec\Tworzenieweb\SoloPlanner\Application\Command;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Application\Command
 */
class DeleteActivityCommandSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Tworzenieweb\SoloPlanner\Application\Command\DeleteActivityCommand');
    }

    function let()
    {
        $this->setId(1)->shouldReturn($this);
        $this->setUserId(2)->shouldReturn($this);
    }

    function it_should_have_assigned_values()
    {
        $this->getId()->shouldReturn(1);
        $this->getUserId()->shouldReturn(2);
    }
}
