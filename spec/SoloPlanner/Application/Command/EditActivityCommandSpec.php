<?php

namespace spec\Tworzenieweb\SoloPlanner\Application\Command;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Application\Command\EditActivityCommand;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Application\Command
 * @mixin EditActivityCommand
 */
class EditActivityCommandSpec extends ObjectBehavior
{
    private $startDate = '2016-01-01';

    private $startTime = '08:00';

    private $endTime = '11:00';

    private $categories = [1, 2, 3];

    private $name = 'Some activity';

    private $note = 'Some note';


    function let()
    {
        $this->setActivityId(1)->shouldReturn($this);
        $this->setUserId(2)->shouldReturn($this);
        $this->setStartAtDate($this->startDate)->shouldReturn($this);
        $this->setStartTime($this->startTime)->shouldReturn($this);
        $this->setEndTime($this->endTime)->shouldReturn($this);
        $this->setName($this->name)->shouldReturn($this);
        $this->setNote($this->note)->shouldReturn($this);
        $this->setCategoryIds($this->categories)->shouldReturn($this);
    }

    function it_assigns_values()
    {
        $this->getActivityId()->shouldReturn(1);
        $this->getUserId()->shouldReturn(2);
        $this->getStartAtDate()->shouldReturn($this->startDate);
        $this->getStartTime()->shouldReturn($this->startTime);
        $this->getEndTime()->shouldReturn($this->endTime);
        $this->getName()->shouldReturn($this->name);
        $this->getNote()->shouldReturn($this->note);
        $this->getCategoryIds()->shouldReturn($this->categories);
    }
}
