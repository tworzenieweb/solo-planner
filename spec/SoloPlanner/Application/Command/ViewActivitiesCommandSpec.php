<?php

namespace spec\Tworzenieweb\SoloPlanner\Application\Command;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Application\Command
 */
class ViewActivitiesCommandSpec extends ObjectBehavior
{
    function let()
    {
        $this->setView('day')->shouldReturn($this);
        $this->setTimeplanId(1)->shouldReturn($this);
        $this->setContext('2016-01-01')->shouldReturn($this);
    }

    function it_assigns_values()
    {
        $this->getView()->shouldReturn('day');
        $this->getTimeplanId()->shouldReturn(1);
        $this->getContext()->shouldReturn('2016-01-01');
    }
}
