<?php

namespace spec\Tworzenieweb\SoloPlanner\Application\Handler;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Application\Command\CreateActivityCommand;
use Tworzenieweb\SoloPlanner\Domain\Category;
use Tworzenieweb\SoloPlanner\Domain\Category\FilterByIdsCategorySpecification;
use Tworzenieweb\SoloPlanner\Domain\CategoryRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\MetaData;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Timeframe;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\TimeplanRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Application\Handler
 */
class CreateActivityHandlerSpec extends ObjectBehavior
{
    function let(
        TimeplanRepository $timeplanRepository,
        ActivityRepository $activityRepository,
        CategoryRepository $categoryRepository,
        FilterByIdsCategorySpecification $categorySpecification
    ) {
        $this->beConstructedWith(
            $timeplanRepository,
            $activityRepository,
            $categoryRepository,
            $categorySpecification
        );
    }

    function it_should_handle_creating_activity_command(
        CreateActivityCommand $command,
        TimeplanRepository $timeplanRepository,
        ActivityRepository $activityRepository,
        CategoryRepository $categoryRepository,
        FilterByIdsCategorySpecification $categorySpecification,
        Timeplan $timeplan,
        Category $category,
        ActivityId $activityId,
        Activity $activity
    )
    {
        $timeplanRepository->ofId(TimeplanId::valueOf(1))->willReturn($timeplan);
        $activityRepository->nextIdentity()->willReturn($activityId);

        $categorySpecification->filterByIds([1])->willReturn($categorySpecification);
        $categoryRepository->match($categorySpecification)->willReturn([$category]);

        $timeplan->makeActivity(
            Argument::type(ActivityId::class),
            Argument::type(Timeframe::class),
            Argument::type(MetaData::class),
            Argument::type(CategoriesCollection::class)
        )->willReturn($activity);
        $activityRepository->add(Argument::type(Activity::class))->shouldBeCalled();

        $this->initializeCommand($command);
        $this->handleCreateActivityCommand($command);
    }

    function it_should_fail_if_timeplan_is_not_available(
        CreateActivityCommand $command,
        TimeplanRepository $timeplanRepository
    )
    {
        $timeplanRepository->ofId(TimeplanId::valueOf(1))->willReturn(null);
        $this->initializeCommand($command);
        $this->shouldThrow(TimeplanNotFoundException::class)->duringHandleCreateActivityCommand($command);
    }

    /**
     * @param CreateActivityCommand $command
     */
    public function initializeCommand(CreateActivityCommand $command)
    {
        $command->getTimeplanId()->willReturn(1);
        $command->getStartDate()->willReturn('2016-01-01');
        $command->getStartTime()->willReturn('10:00');
        $command->getEndTime()->willReturn('12:00');
        $command->getName()->willReturn('name');
        $command->getNote()->willReturn('note');
        $command->getCategoryIds()->willReturn([1]);
    }
}
