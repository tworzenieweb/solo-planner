<?php

namespace spec\Tworzenieweb\SoloPlanner\Application\Handler;

use Broadway\CommandHandling\CommandHandler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Application\Command\DeleteActivityCommand;
use Tworzenieweb\SoloPlanner\Application\Handler\DeleteActivityHandler;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityCannotBeDeletedException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Domain\User;
use Tworzenieweb\SoloPlanner\Domain\User\UserId;
use Tworzenieweb\SoloPlanner\Domain\User\UserNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\UserRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Application\Handler
 * @mixin DeleteActivityHandler
 */
class DeleteActivityHandlerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldImplement(CommandHandler::class);
    }

    function let(
        ActivityRepository $activityRepository,
        UserRepository $userRepository
    ) {
        $this->beConstructedWith(
            $activityRepository,
            $userRepository
        );
    }

    function it_should_handle_delete_activity_command(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        DeleteActivityCommand $command,
        User $user,
        Activity $activity
    )
    {
        $this->initializeCommand($command);
        $userRepository->ofId(UserId::valueOf(2))->willReturn($user);
        $activityRepository->ofId(ActivityId::valueOf(1))->willReturn($activity);
        $user->canDelete($activity)->willReturn(true);
        $activityRepository->remove($activity)->shouldBeCalled();

        $this->handleDeleteActivityCommand($command);
    }

    function it_should_fail_when_user_was_not_found(
        UserRepository $userRepository,
        DeleteActivityCommand $command
    )
    {
        $this->initializeCommand($command);
        $userRepository->ofId(Argument::any())->willReturn(null);
        $this->shouldThrow(UserNotFoundException::class)->duringHandleDeleteActivityCommand($command);
    }


    function it_should_fail_when_activity_was_not_found(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        DeleteActivityCommand $command,
        User $user
    )
    {
        $this->initializeCommand($command);
        $userRepository->ofId(UserId::valueOf(2))->willReturn($user);
        $activityRepository->ofId(ActivityId::valueOf(1))->willReturn(null);

        $this->shouldThrow(ActivityNotFoundException::class)->duringHandleDeleteActivityCommand($command);
    }

    function it_should_fail_when_user_cannot_delete_activity(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        DeleteActivityCommand $command,
        User $user,
        Activity $activity
    )
    {
        $this->initializeCommand($command);
        $userRepository->ofId(UserId::valueOf(2))->willReturn($user);
        $activityRepository->ofId(ActivityId::valueOf(1))->willReturn($activity);
        $user->canDelete($activity)->willReturn(false);

        $this->shouldThrow(ActivityCannotBeDeletedException::class)->duringHandleDeleteActivityCommand($command);
    }

    /**
     * @param DeleteActivityCommand $command
     */
    private function initializeCommand(DeleteActivityCommand $command)
    {
        $command->getId()->willReturn(1);
        $command->getUserId()->willReturn(2);
    }
}
