<?php

namespace spec\Tworzenieweb\SoloPlanner\Application\Handler;

use Broadway\CommandHandling\CommandHandler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Application\Command\EditActivityCommand;
use Tworzenieweb\SoloPlanner\Application\Handler\EditActivityHandler;
use Tworzenieweb\SoloPlanner\Domain\Category;
use Tworzenieweb\SoloPlanner\Domain\Category\FilterByIdsCategorySpecification;
use Tworzenieweb\SoloPlanner\Domain\CategoryRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityCannotBeEditedException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time;
use Tworzenieweb\SoloPlanner\Domain\User;
use Tworzenieweb\SoloPlanner\Domain\User\UserId;
use Tworzenieweb\SoloPlanner\Domain\User\UserNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\UserRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Application\Handler
 * @mixin EditActivityHandler
 */
class EditActivityHandlerSpec extends ObjectBehavior
{
    const EXAMPLE_CATEGORIES = [1];

    function it_is_initializable()
    {
        $this->shouldImplement(CommandHandler::class);
    }

    function let(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        CategoryRepository $categoryRepository,
        FilterByIdsCategorySpecification $categorySpecification
    ) {
        $this->beConstructedWith(
            $activityRepository,
            $userRepository,
            $categoryRepository,
            $categorySpecification
        );
    }

    function it_should_handle_edit_activity_command(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        EditActivityCommand $command,
        User $user,
        Activity $activity,
        Category $category,
        CategoryRepository $categoryRepository,
        FilterByIdsCategorySpecification $categorySpecification
    ) {
        $this->initializeCommand($command);
        $userRepository->ofId(UserId::valueOf(2))->willReturn($user);
        $activityRepository->ofId(ActivityId::valueOf(1))->willReturn($activity);
        $user->canEdit($activity)->willReturn(true);
        $categorySpecification->filterByIds(self::EXAMPLE_CATEGORIES)->willReturn($categoryRepository);
        $categoryRepository->match($categorySpecification)->willReturn([$category]);

        $activity->changeName(Argument::type('string'))->shouldBeCalled();
        $activity->changeNote(Argument::type('string'))->shouldBeCalled();
        $activity->changeStartTime(Argument::type(Time::class))->shouldBeCalled();
        $activity->changeEndTime(Argument::type(Time::class))->shouldBeCalled();
        $activity->changeStartAtDate(Argument::type(Date::class))->shouldBeCalled();
        $activity->changeCategories(Argument::type(CategoriesCollection::class))->shouldBeCalled();

        $this->handleEditActivityCommand($command);
    }

    function it_should_handle_Edit_activity_command_with_no_action(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        EditActivityCommand $command,
        User $user,
        Activity $activity
    ) {
        $this->initializeEmptyCommand($command);
        $userRepository->ofId(UserId::valueOf(2))->willReturn($user);
        $activityRepository->ofId(ActivityId::valueOf(1))->willReturn($activity);
        $user->canEdit($activity)->willReturn(true);

        $activity->changeName(Argument::type('string'))->shouldNotBeCalled();
        $activity->changeNote(Argument::type('string'))->shouldNotBeCalled();
        $activity->changeStartTime(Argument::type(Time::class))->shouldNotBeCalled();
        $activity->changeEndTime(Argument::type(Time::class))->shouldNotBeCalled();
        $activity->changeStartAtDate(Argument::type(Date::class))->shouldNotBeCalled();
        $activity->changeCategories(Argument::type(CategoriesCollection::class))->shouldNotBeCalled();

        $this->handleEditActivityCommand($command);
    }

    function it_should_fail_when_user_was_not_found(
        UserRepository $userRepository,
        EditActivityCommand $command
    )
    {
        $this->initializeCommand($command);
        $userRepository->ofId(Argument::any())->willReturn(null);
        $this->shouldThrow(UserNotFoundException::class)->duringHandleEditActivityCommand($command);
    }


    function it_should_fail_when_activity_was_not_found(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        EditActivityCommand $command,
        User $user
    )
    {
        $this->initializeCommand($command);
        $userRepository->ofId(UserId::valueOf(2))->willReturn($user);
        $activityRepository->ofId(ActivityId::valueOf(1))->willReturn(null);

        $this->shouldThrow(ActivityNotFoundException::class)->duringHandleEditActivityCommand($command);
    }

    function it_should_fail_when_user_cannot_edit_activity(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        EditActivityCommand $command,
        User $user,
        Activity $activity
    )
    {
        $this->initializeCommand($command);
        $userRepository->ofId(UserId::valueOf(2))->willReturn($user);
        $activityRepository->ofId(ActivityId::valueOf(1))->willReturn($activity);
        $user->canEdit($activity)->willReturn(false);

        $this->shouldThrow(ActivityCannotBeEditedException::class)->duringHandleEditActivityCommand($command);
    }

    /**
     * @param EditActivityCommand $command
     */
    private function initializeCommand(EditActivityCommand $command)
    {
        $command->getUserId()->willReturn(2);
        $command->getActivityId()->willReturn(1);
        $command->getStartAtDate()->willReturn('2016-01-01');
        $command->getStartTime()->willReturn('10:00');
        $command->getEndTime()->willReturn('12:00');
        $command->getName()->willReturn('name');
        $command->getNote()->willReturn('note');
        $command->getCategoryIds()->willReturn(self::EXAMPLE_CATEGORIES);
    }

    /**
     * @param EditActivityCommand $command
     */
    private function initializeEmptyCommand(EditActivityCommand $command)
    {
        $command->getUserId()->willReturn(2);
        $command->getActivityId()->willReturn(1);
        $command->getStartAtDate()->willReturn(null);
        $command->getStartTime()->willReturn(null);
        $command->getEndTime()->willReturn(null);
        $command->getName()->willReturn(null);
        $command->getNote()->willReturn(null);
        $command->getCategoryIds()->willReturn(null);
    }
}
