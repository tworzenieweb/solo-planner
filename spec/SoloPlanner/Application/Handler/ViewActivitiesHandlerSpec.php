<?php

namespace spec\Tworzenieweb\SoloPlanner\Application\Handler;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Application\Command\ViewActivitiesCommand;
use Tworzenieweb\SoloPlanner\Application\ViewDTO;
use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\DateContext;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\DateContextActivitySpecification;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\TimeplanRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Application\Handler
 */
class ViewActivitiesHandlerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Tworzenieweb\SoloPlanner\Application\Handler\ViewActivitiesHandler');
    }

    function let(
        TimeplanRepository $timeplanRepository,
        ActivityRepository $activityRepository,
        DateContextActivitySpecification $specification,
        ViewDTO $viewDTO
    )
    {
        $this->beConstructedWith(
            $timeplanRepository,
            $activityRepository,
            $specification,
            $viewDTO
        );
    }

    function it_should_handle_view_activities_command(
        ViewActivitiesCommand $command,
        TimeplanRepository $timeplanRepository,
        ActivityRepository $activityRepository,
        DateContextActivitySpecification $specification,
        Timeplan $timeplan,
        ActivityId $activityId
    )
    {
        $timeplanRepository->ofId(TimeplanId::valueOf(1))->willReturn($timeplan);
        $activityRepository->nextIdentity()->willReturn($activityId);

        $specification->filterByDateContext(Argument::type(DateContext::class))->shouldBeCalled();
        $activityRepository->match($specification)->shouldBeCalled();

        $this->initializeCommand($command);
        $this->handleViewActivitiesCommand($command);
    }

    function it_should_fail_if_timeplan_is_not_available(ViewActivitiesCommand $command,
        TimeplanRepository $timeplanRepository
    )
    {
        $timeplanRepository->ofId(TimeplanId::valueOf(1))->willReturn(null);
        $this->initializeCommand($command);
        $this->shouldThrow(TimeplanNotFoundException::class)->duringHandleViewActivitiesCommand($command);
    }

    /**
     * @param ViewActivitiesCommand $command
     */
    private function initializeCommand(ViewActivitiesCommand $command)
    {
        $command->getTimeplanId()->willReturn(1);
        $command->getContext()->willReturn('2016-01-01');
        $command->getView()->willReturn('day');
    }
}
