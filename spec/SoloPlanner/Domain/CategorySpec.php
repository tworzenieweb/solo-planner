<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Category\CategoryId;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain
 */
class CategorySpec extends ObjectBehavior
{
    const CATEGORY_NAME = 'category name';

    function let(CategoryId $categoryId)
    {
        $this->beConstructedWith($categoryId, self::CATEGORY_NAME);
    }

    function it_should_have_name()
    {
        $this->getName()->shouldReturn(self::CATEGORY_NAME);
    }

    function it_should_have_id()
    {
        $this->getId()->shouldHaveType(CategoryId::class);
    }

    function it_should_have_to_string_method()
    {
        $this->__toString()->shouldReturn(self::CATEGORY_NAME);
    }
}
