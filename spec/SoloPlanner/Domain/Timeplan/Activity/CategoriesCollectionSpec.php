<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Category;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\EmptyCategoriesCollectionException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\InvalidCategoriesCollectionException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class CategoriesCollectionSpec extends ObjectBehavior
{
    function let(Category $category)
    {
        $this->beConstructedThrough('fromArray', [[$category]]);
    }

    function it_should_throw_exception_when_list_is_empty()
    {
        $this->beConstructedThrough('fromArray', [[]]);
        $this->shouldThrow(EmptyCategoriesCollectionException::class)->duringInstantiation();
    }

    function it_should_throw_exception_when_list_is_not_consistent(Category $category)
    {
        $this->beConstructedThrough('fromArray', [[$category, "aaa", "bbb"]]);
        $this->shouldThrow(InvalidCategoriesCollectionException::class)->duringInstantiation();
    }

    function it_should_allow_to_add_category_and_return_new_collection(Category $category)
    {
        $this->add($category)->shouldReturnAnInstanceOf(CategoriesCollection::class);
        $newCollection = $this->add($category)->shouldNotBe($this);
        $this->shouldHaveLessElementsThanNewCollection($newCollection);
    }

    function it_should_have_to_string_method(Category $category)
    {
        $category->__toString()->willReturn('1');
        $this->__toString()->shouldReturn('1');
    }

    function getMatchers()
    {
        return [
            'haveLessElementsThanNewCollection' => function (CategoriesCollection $subject, CategoriesCollection $new) {
                return count($subject->getCategories()) < count($new->getCategories());
            },
        ];
    }
}
