<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\DateContext;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\InvalidDateContextException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class DayDateContextSpec extends ObjectBehavior
{
    function it_can_be_a_day()
    {
        $this->beConstructedThrough('fromViewAndContext', [DateContext::VIEW_DAY, '2016-01-01']);

        $this->getStartDate()->format('Y-m-d')->shouldBe('2016-01-01');
        $this->getEndDate()->format('Y-m-d')->shouldBe('2016-01-01');
    }

    function it_can_be_a_week()
    {
        $this->beConstructedThrough('fromViewAndContext', [DateContext::VIEW_WEEK, '2016/W01']);

        $this->getStartDate()->format('Y-m-d')->shouldBe('2016-01-04');
        $this->getEndDate()->format('Y-m-d')->shouldBe('2016-01-10');
    }

    function it_can_be_a_month()
    {
        $this->beConstructedThrough('fromViewAndContext', [DateContext::VIEW_MONTH, '01/2016']);

        $this->getStartDate()->format('Y-m-d')->shouldBe('2016-01-01');
        $this->getEndDate()->format('Y-m-d')->shouldBe('2016-01-31');
    }

    function it_can_be_a_year()
    {
        $this->beConstructedThrough('fromViewAndContext', [DateContext::VIEW_YEAR, '2016']);

        $this->getStartDate()->format('Y-m-d')->shouldBe('2016-01-01');
        $this->getEndDate()->format('Y-m-d')->shouldBe('2016-12-31');
    }

    function it_should_fail_for_unsupported_view()
    {
        $this->beConstructedThrough('fromViewAndContext', ['notExistingView', '2016']);
        $this->shouldThrow(InvalidDateContextException::class)->duringInstantiation();
    }
}
