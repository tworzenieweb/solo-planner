<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\MetaData;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 * @mixin  MetaData
 */
class MetaDataSpec extends ObjectBehavior
{
    const NAME = 'some name';
    const NOTE = 'some note';

    function let()
    {
        $this->beConstructedWith(self::NAME, self::NOTE);
    }

    function it_should_return_name_and_note()
    {
        $this->getName()->shouldReturn(self::NAME);
        $this->getNote()->shouldReturn(self::NOTE);
    }

    function it_should_change_name_and_return_new_object()
    {
        $otherValue = 'other name';
        $newMetaData = $this->changeName($otherValue);
        $newMetaData->shouldHaveType(MetaData::class);
        $newMetaData->getName()->shouldReturn($otherValue);
    }

    function it_should_change_note_and_return_new_object()
    {
        $otherValue = 'other note';
        $newMetaData = $this->changeNote($otherValue);
        $newMetaData->shouldHaveType(MetaData::class);
        $newMetaData->getNote()->shouldReturn($otherValue);
    }


    function it_should_have_to_string_method()
    {
        $this->__toString()->shouldReturn(sprintf("%s\n%s", self::NAME, self::NOTE));
    }
}
