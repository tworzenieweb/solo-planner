<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ActivityIdSpec extends ObjectBehavior
{
    const EXAMPLE_ID = 'someId';

    function it_can_be_initialized_from_string()
    {
        $this->beConstructedThrough('valueOf', [self::EXAMPLE_ID]);
        $this->getId()->shouldReturn(self::EXAMPLE_ID);
    }
}
