<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan;

use PhpSpec\ObjectBehavior;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\MetaData;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Timeframe;
use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\User;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain\Timeplan
 * @mixin Timeplan\Activity
 */
class ActivitySpec extends ObjectBehavior
{
    const EXAMPLE_NAME = 'Test';
    const EXAMPLE_NOTE = 'Test note';

    function let(
        ActivityId $activityId,
        Timeframe $timeframe,
        Timeplan $timeplan,
        MetaData $metaData,
        CategoriesCollection $categories,
        User $user,
        Date $startAtDate,
        Time $startTime,
        Time $endTime
    )
    {
        $metaData->getName()->willReturn(self::EXAMPLE_NAME);
        $metaData->getNote()->willReturn(self::EXAMPLE_NOTE);
        $timeframe->getDate()->willReturn($startAtDate);
        $timeplan->getUser()->willReturn($user);
        $timeframe->getStartTime()->willReturn($startTime);
        $timeframe->getEndTime()->willReturn($endTime);
        $this->beConstructedWith($activityId, $timeframe, $timeplan, $metaData, $categories);
    }

    function it_should_return_id(ActivityId $activityId)
    {
        $this->getId()->shouldReturn($activityId);
    }

    function it_should_return_timeframe_data(Date $startAtDate, Time $startTime, Time $endTime)
    {
        $this->getStartAtDate()->shouldReturn($startAtDate);
        $this->getStartTime()->shouldReturn($startTime);
        $this->getEndTime()->shouldReturn($endTime);
    }

    function it_should_have_name_and_description()
    {
        $this->getName()->shouldReturn(self::EXAMPLE_NAME);
        $this->getNote()->shouldReturn(self::EXAMPLE_NOTE);
    }

    function it_should_have_a_list_of_categories(CategoriesCollection $categories)
    {
        $this->getCategories()->shouldReturn($categories);
    }

    function it_should_get_user(User $user)
    {
        $this->getUser()->shouldReturn($user);
    }

    function it_should_return_timeplan(Timeplan $timeplan)
    {
        $this->getTimeplan()->shouldReturn($timeplan);
    }

    function it_should_change_meta_name(MetaData $metaData, MetaData $otherMetaData)
    {
        $this->getName()->shouldReturn(self::EXAMPLE_NAME);

        $otherName = 'other name';
        $otherMetaData->getName()->willReturn($otherName);
        $metaData->changeName($otherName)->willReturn($otherMetaData);

        $this->changeName($otherName)->shouldReturn($this);
        $this->getName()->shouldReturn($otherName);
    }

    function it_should_change_meta_description(MetaData $metaData, MetaData $otherMetaData)
    {
        $this->getNote()->shouldReturn(self::EXAMPLE_NOTE);

        $otherNote = 'note';
        $otherMetaData->getNote()->willReturn($otherNote);
        $metaData->changeNote($otherNote)->willReturn($otherMetaData);

        $this->changeNote($otherNote)->shouldReturn($this);
        $this->getNote()->shouldReturn($otherNote);
    }

    function it_should_change_start_date(Date $startAtDate, Timeframe $timeframe, Timeframe $newTimeframe)
    {
        $timeframe->changeStartAtDate($startAtDate)->willReturn($newTimeframe);
        $newTimeframe->getDate()->willReturn($startAtDate);
        $this->changeStartAtDate($startAtDate);
        $this->getStartAtDate()->shouldReturn($startAtDate);
    }

    function it_should_change_start_time(Time $newStartTime, Timeframe $timeframe, Timeframe $newTimeframe)
    {
        $timeframe->changeStartTime($newStartTime)->willReturn($newTimeframe);
        $newTimeframe->getStartTime()->willReturn($newStartTime);
        $this->changeStartTime($newStartTime);
        $this->getStartTime()->shouldReturn($newStartTime);
    }

    function it_should_change_end_time(Time $newEndTime, Timeframe $timeframe, Timeframe $newTimeframe)
    {
        $timeframe->changeEndTime($newEndTime)->willReturn($newTimeframe);
        $newTimeframe->getEndTime()->willReturn($newEndTime);
        $this->changeEndTime($newEndTime);
        $this->getEndTime()->shouldReturn($newEndTime);
    }

    function it_should_change_categories(CategoriesCollection $categoriesCollection)
    {
        $this->changeCategories($categoriesCollection);
        $this->getCategories()->shouldReturn($categoriesCollection);
    }
}
