<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date\InvalidArgumentException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class DateSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('2016-01-01');
    }

    function it_should_throw_domain_exception()
    {
        $this->beConstructedWith('definitelly not a date');
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }


    function it_should_have_to_string_method()
    {
        $this->__toString()->shouldReturn('2016-01-01 00:00:00');
    }
}
