<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time\InvalidArgumentException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class TimeSpec extends ObjectBehavior
{
    const EXAMPLE_TIME = '10:00';

    function let()
    {
        $this->beConstructedThrough('valueOf', [self::EXAMPLE_TIME]);
    }

    function it_should_fail_on_invalid_arguments()
    {
        $this->beConstructedThrough('valueOf', ['aaa']);

        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_should_have_to_string_method()
    {
        $this->__toString()->shouldReturn(self::EXAMPLE_TIME);
    }
}
