<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Timeframe;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain\Timeplan
 * @mixin Timeframe
 */
class TimeframeSpec extends ObjectBehavior
{
    const START_DATE = '2016-01-01';

    const START_TIME = '10:00';

    const END_TIME = '14:00';

    /**
     * @var Date
     */
    private $date;

    /**
     * @var Time
     */
    private $fromTime;

    /**
     * @var Time
     */
    private $toTime;

    function let()
    {
        $this->date = new Date(self::START_DATE);
        $this->fromTime = Time::valueOf(self::START_TIME);
        $this->toTime = Time::valueOf(self::END_TIME);

        $this->beConstructedThrough('fromDateAndTimes', [
            $this->date,
            $this->fromTime,
            $this->toTime
        ]);
    }

    function it_should_have_data_assigned()
    {
        $this->getDate()->shouldBe($this->date);
        $this->getStartTime()->shouldBe($this->fromTime);
        $this->getEndTime()->shouldBe($this->toTime);
    }

    function it_should_change_start_date_by_creating_new(Date $date)
    {
        $newTimeframe = $this->changeStartAtDate($date);

        $this->shouldNotBeEqualTo($newTimeframe);
        $newTimeframe->getDate()->shouldBe($date);
    }

    function it_should_change_start_time_by_creating_new(Time $startTime)
    {
        $newTimeframe = $this->changeStartTime($startTime);

        $this->shouldNotBeEqualTo($newTimeframe);
        $newTimeframe->getStartTime()->shouldBe($startTime);
    }

    function it_should_change_end_time_by_creating_new(Time $endTime)
    {
        $newTimeframe = $this->changeEndTime($endTime);

        $this->shouldNotBeEqualTo($newTimeframe);
        $newTimeframe->getEndTime()->shouldBe($endTime);
    }

    function it_should_have_to_string_method()
    {
        $this->__toString()->shouldReturn(sprintf('%s 00:00:00: %s - %s', self::START_DATE, self::START_TIME, self::END_TIME));
    }
}
