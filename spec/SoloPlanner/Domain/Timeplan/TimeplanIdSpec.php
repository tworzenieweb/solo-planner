<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain\Timeplan;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TimeplanIdSpec extends ObjectBehavior
{

    function it_should_have_to_string_method()
    {
        $this->beConstructedThrough('valueOf', [1]);

        $this->__toString()->shouldReturn(1);
    }
}
