<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityAlreadyExistsException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\MetaData;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Timeframe;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;
use Tworzenieweb\SoloPlanner\Domain\User;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain\Timeplan
 * @mixin  Timeplan
 */
class TimeplanSpec extends ObjectBehavior
{
    function let(TimeplanId $id, User $user)
    {
        $this->beConstructedWith($id, $user);
    }

    function it_should_allow_to_add_activity(
        ActivityId $activityId,
        Timeframe $timeframe,
        MetaData $metaData,
        CategoriesCollection $collection
    ) {
        $this->makeActivity($activityId, $timeframe, $metaData, $collection)
             ->shouldHaveType(Activity::class);
    }

    function it_should_get_id(TimeplanId $id)
    {
        $this->getId()->shouldReturn($id);
    }

    function it_should_get_user(User $user)
    {
        $this->getUser()->shouldReturn($user);
    }

    function it_shouldnt_allow_to_add_the_same_activity_twice(
        ActivityId $activityId,
        Timeframe $timeframe,
        MetaData $metaData,
        CategoriesCollection $collection
    )
    {
        $activityId->__toString()->willReturn('1');
        $timeframe->__toString()->willReturn('2016-01-01 08:00');
        $metaData->__toString()->willReturn('some name, somedesc');
        $collection->__toString()->willReturn('1, 2, 3');

        $this->makeActivity($activityId, $timeframe, $metaData, $collection);

        $this->shouldThrow(ActivityAlreadyExistsException::class)
             ->duringMakeActivity($activityId, $timeframe, $metaData, $collection);

        $this->getActivities()->shouldHaveCount(1);
        $this->getUncommittedEvents()->getIterator()->count()->shouldBe(1);
    }
}
