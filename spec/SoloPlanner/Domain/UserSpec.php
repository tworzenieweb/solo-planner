<?php

namespace spec\Tworzenieweb\SoloPlanner\Domain;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\User;
use Tworzenieweb\SoloPlanner\Domain\User\UserId;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Domain
 */
class UserSpec extends ObjectBehavior
{

    function let(UserId $userId)
    {
        $this->beConstructedWith($userId);
    }

    function it_should_allow_to_create_timeplans()
    {
        $this->createTimeplan()->shouldReturnAnInstanceOf(Timeplan::class);
    }

    function it_should_get_id(UserId $userId)
    {
        $this->getId()->shouldReturn($userId);
    }

    function it_should_check_for_deletion_permission_for_activity(Activity $activity, User $otherUser)
    {
        $activity->getUser()->willReturn($this->getWrappedObject());
        $this->canDelete($activity)->shouldReturn(true);

        $activity->getUser()->willReturn($otherUser);
        $this->canDelete($activity)->shouldReturn(false);
    }

    function it_should_check_for_edit_permission_for_activity(Activity $activity, User $otherUser)
    {
        $activity->getUser()->willReturn($this->getWrappedObject());
        $this->canEdit($activity)->shouldReturn(true);

        $activity->getUser()->willReturn($otherUser);
        $this->canEdit($activity)->shouldReturn(false);
    }
}
