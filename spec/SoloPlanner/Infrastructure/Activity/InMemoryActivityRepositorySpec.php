<?php

namespace spec\Tworzenieweb\SoloPlanner\Infrastructure\Activity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivitySpecification;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Infrastructure\Activity
 */
class InMemoryActivityRepositorySpec extends ObjectBehavior
{
    function it_implements_activity_repository()
    {
        $this->shouldImplement(ActivityRepository::class);
    }

    function it_should_add_activity(Activity $activity)
    {
        $activity->getId()->willReturn($this->nextIdentity());
        $this->add($activity)->shouldReturn($this);
    }

    function it_should_find_activity_by_id(Activity $firstActivity, Activity $secondActivity)
    {
        $wrappedObject = $this->getWrappedObject();
        $firstId = $wrappedObject->nextIdentity();
        $secondId = $wrappedObject->nextIdentity();

        $firstActivity->getId()->willReturn($firstId);
        $secondActivity->getId()->willReturn($secondId);

        $this->add($firstActivity)
             ->add($secondActivity);

        $this->ofId($firstId)->shouldReturn($firstActivity);
        $this->ofId($secondId)->shouldReturn($secondActivity);

        $this->getAll()->shouldReturn([
            $firstId->getId() => $firstActivity,
            $secondId->getId() => $secondActivity
        ]);
    }

    function it_should_find_activity_by_name(Activity $firstActivity, Activity $secondActivity)
    {
        $wrappedObject = $this->getWrappedObject();
        $firstId = $wrappedObject->nextIdentity();
        $secondId = $wrappedObject->nextIdentity();

        $firstActivity->getId()->willReturn($firstId);
        $secondActivity->getId()->willReturn($secondId);

        $firstActivity->getName()->willReturn('test');
        $secondActivity->getName()->willReturn('other');

        $this->add($firstActivity)
            ->add($secondActivity);

        $this->ofName('test')->shouldReturn([$firstActivity]);
    }

    function it_should_return_results_based_on_specification(ActivitySpecification $specification)
    {
        $specification->match($this)->shouldBeCalled();
        $this->match($specification);
    }

    function it_should_return_null_when_activity_was_not_found(ActivityId $activityId)
    {
        $this->ofId($activityId)->shouldReturn(null);
    }

    function it_should_purge_elements(Activity $activity)
    {
        $activityId = ActivityId::getNextId();
        $activity->getId()->willReturn($activityId);
        $this->add($activity);
        $this->ofId($activityId)->shouldReturn($activity);
        $this->purge();
        $this->ofId($activityId)->shouldReturn(null);
    }

    function it_should_remove_element(Activity $activity)
    {
        $activityId = ActivityId::getNextId();
        $activity->getId()->willReturn($activityId);
        $this->add($activity);
        $this->ofId($activityId)->shouldReturn($activity);
        $this->remove($activity)->shouldReturn($this);
        $this->ofId($activityId)->shouldReturn(null);
    }
}
