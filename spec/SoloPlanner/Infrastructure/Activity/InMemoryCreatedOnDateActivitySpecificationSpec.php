<?php

namespace spec\Tworzenieweb\SoloPlanner\Infrastructure\Activity;

use InvalidArgumentException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivitySpecification;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\DateContext;
use Tworzenieweb\SoloPlanner\Infrastructure\Activity\InMemoryActivityRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Infrastructure\Activity
 */
class InMemoryCreatedOnDateActivitySpecificationSpec extends ObjectBehavior
{
    function it_implements_specification_interface()
    {
        $this->shouldImplement(ActivitySpecification::class);
    }

    function it_should_fail_if_context_was_not_set(InMemoryActivityRepository $repository)
    {
        $this->shouldThrow(InvalidArgumentException::class)->duringMatch($repository);
    }

    function it_should_filter_by_date(InMemoryActivityRepository $repository, Activity $a1, Activity $a2)
    {
        $forDate = '2016-01-01';
        $this->filterByDateContext(DateContext::fromViewAndContext(DateContext::VIEW_DAY, $forDate));

        $a1->getStartAtDate()->willReturn(new Date('2016-01-05'));
        $a2->getStartAtDate()->willReturn(new Date('2016-01-01'));

        $repository->getAll()->willReturn([
            $a1,
            $a2
        ]);

        $this->match($repository)->shouldReturn([$a2]);
    }
}
