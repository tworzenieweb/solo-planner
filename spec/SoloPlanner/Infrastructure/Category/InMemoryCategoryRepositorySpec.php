<?php

namespace spec\Tworzenieweb\SoloPlanner\Infrastructure\Category;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Category;
use Tworzenieweb\SoloPlanner\Domain\Category\CategoryId;
use Tworzenieweb\SoloPlanner\Domain\Category\CategorySpecification;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Infrastructure\Category
 */
class InMemoryCategoryRepositorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldImplement('Tworzenieweb\SoloPlanner\Domain\CategoryRepository');
    }

    function it_should_add_category(Category $category)
    {
        $categoryId = $this->nextIdentity();
        $category->getId()->willReturn($categoryId);
        $this->add($category)->shouldReturn($this);
        $this->ofId($categoryId)->shouldReturn($category);
    }

    function it_should_find_category_by_id(Category $category, Category $otherCategory)
    {
        $otherId = $this->nextIdentity();
        $id = $this->nextIdentity();

        $category->getId()->willReturn($id);
        $otherCategory->getId()->willReturn($otherId);

        $this->add($category)->shouldReturn($this);
        $this->add($otherCategory)->shouldReturn($this);

        $this->ofId($id)->shouldReturn($category);
        $this->ofId($otherId)->shouldReturn($otherCategory);
    }

    function it_should_give_null_when_category_was_not_found(CategoryId $categoryId)
    {
        $this->ofId($categoryId)->shouldReturn(null);
    }

    function it_should_return_unique_ids()
    {
        $this->nextIdentity()->shouldNotBeEqualTo($this->nextIdentity());
    }

    function it_should_return_results_based_on_specification(CategorySpecification $specification)
    {
        $specification->match($this)->shouldBeCalled();
        $this->match($specification);
    }

    function it_should_return_all_elements(Category $c1, Category $c2)
    {
        $c1->getId()->willReturn(CategoryId::getNextId());
        $c2->getId()->willReturn(CategoryId::getNextId());
        $this->add($c1);
        $this->add($c2);

        $this->getAll()->shouldHaveCount(2);
    }
}
