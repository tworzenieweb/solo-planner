<?php

namespace spec\Tworzenieweb\SoloPlanner\Infrastructure\Category;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Category;
use Tworzenieweb\SoloPlanner\Domain\Category\CategoryId;
use Tworzenieweb\SoloPlanner\Domain\Category\FilterByIdsCategorySpecification;
use Tworzenieweb\SoloPlanner\Infrastructure\Category\InMemoryCategoryRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Infrastructure\Category
 */
class InMemoryFilterByIdsCategorySpecificationSpec extends ObjectBehavior
{
    function it_implements_specification_interface()
    {
        $this->shouldImplement(FilterByIdsCategorySpecification::class);
    }

    function it_should_filter_by_ids(InMemoryCategoryRepository $repository, Category $a1, Category $a2)
    {
        $id1 = CategoryId::getNextId();
        $id2 = CategoryId::getNextId();

        $a1->getId()->willReturn($id1);
        $a2->getId()->willReturn($id2);

        $repository->getAll()->willReturn([
            $id1->getId() => $a1,
            $id2->getId() => $a2
        ]);

        $this->filterByIds([$id2->getId()]);
        $this->match($repository)->shouldReturn([$a2]);
    }
}
