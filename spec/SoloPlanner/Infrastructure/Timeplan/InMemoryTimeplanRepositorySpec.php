<?php

namespace spec\Tworzenieweb\SoloPlanner\Infrastructure\Timeplan;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanAlreadyExists;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;
use Tworzenieweb\SoloPlanner\Domain\TimeplanRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Infrastructure\Timeplan
 */
class InMemoryTimeplanRepositorySpec extends ObjectBehavior
{
    function it_implements_activity_repository()
    {
        $this->shouldImplement(TimeplanRepository::class);
    }

    function it_should_add_timeplan(Timeplan $timeplan)
    {
        $timeplan->getId()->willReturn($this->nextIdentity());
        $this->add($timeplan)->shouldReturn($this);
    }

    function it_should_find_timeplan_by_id(Timeplan $firstTimeplan, Timeplan $secondTimeplan)
    {
        $wrappedObject = $this->getWrappedObject();
        $firstId = $wrappedObject->nextIdentity();
        $secondId = $wrappedObject->nextIdentity();

        $firstTimeplan->getId()->willReturn($firstId);
        $secondTimeplan->getId()->willReturn($secondId);

        $this->add($firstTimeplan)
            ->add($secondTimeplan);

        $this->ofId($firstId)->shouldReturn($firstTimeplan);
        $this->ofId($secondId)->shouldReturn($secondTimeplan);

        $this->getAll()->shouldReturn([
            $firstId->getId() => $firstTimeplan,
            $secondId->getId() => $secondTimeplan
        ]);
    }

    function it_should_return_null_when_timeplan_was_not_found(TimeplanId $timeplanId)
    {
        $this->ofId($timeplanId)->shouldReturn(null);
    }

    function it_should_not_allow_to_add_the_same_timeplan_twice(TimeplanId $timeplanId, Timeplan $timeplan)
    {
        $timeplanId->getId()->willReturn(1);
        $timeplan->getId()->willReturn($timeplanId);
        $this->add($timeplan);
        $this->shouldThrow(TimeplanAlreadyExists::class)->duringAdd($timeplan);
    }
}
