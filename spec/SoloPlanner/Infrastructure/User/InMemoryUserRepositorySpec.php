<?php

namespace spec\Tworzenieweb\SoloPlanner\Infrastructure\User;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tworzenieweb\SoloPlanner\Domain\User;
use Tworzenieweb\SoloPlanner\Domain\User\UserAlreadyExistsException;
use Tworzenieweb\SoloPlanner\Domain\User\UserId;
use Tworzenieweb\SoloPlanner\Domain\UserRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package spec\Tworzenieweb\SoloPlanner\Infrastructure\User
 */
class InMemoryUserRepositorySpec extends ObjectBehavior
{
    function it_implements_user_repository()
    {
        $this->shouldImplement(UserRepository::class);
    }

    function it_should_add_user(User $timeplan)
    {
        $timeplan->getId()->willReturn($this->nextIdentity());
        $this->add($timeplan)->shouldReturn($this);
    }

    function it_should_find_user_by_id(User $firstTimeplan, User $secondTimeplan)
    {
        $wrappedObject = $this->getWrappedObject();
        $firstId = $wrappedObject->nextIdentity();
        $secondId = $wrappedObject->nextIdentity();

        $firstTimeplan->getId()->willReturn($firstId);
        $secondTimeplan->getId()->willReturn($secondId);

        $this->add($firstTimeplan)
             ->add($secondTimeplan);

        $this->ofId($firstId)->shouldReturn($firstTimeplan);
        $this->ofId($secondId)->shouldReturn($secondTimeplan);

        $this->getAll()->shouldReturn([
            $firstId->getId() => $firstTimeplan,
            $secondId->getId() => $secondTimeplan
        ]);
    }

    function it_should_return_null_when_user_was_not_found(UserId $userId)
    {
        $this->ofId($userId)->shouldReturn(null);
    }

    function it_should_not_allow_to_add_the_same_user_twice(UserId $userId, User $user)
    {
        $userId->getId()->willReturn(1);
        $user->getId()->willReturn($userId);
        $this->add($user);
        $this->shouldThrow(UserAlreadyExistsException::class)->duringAdd($user);
    }
}
