<?php

namespace Tworzenieweb\SoloPlanner\Application\Command;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application\Command
 */
class CreateActivityCommand
{
    /**
     * @var int
     */
    private $timeplanId;

    /**
     * @var string
     */
    private $startDate;

    /**
     * @var string
     */
    private $startTime;

    /**
     * @var string
     */
    private $endTime;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $note;

    /**
     * @var array
     */
    private $categoryIds;

    /**
     * @return int
     */
    public function getTimeplanId()
    {
        return $this->timeplanId;
    }

    /**
     * @param int $timeplanId
     *
     * @return CreateActivityCommand
     */
    public function setTimeplanId($timeplanId)
    {
        $this->timeplanId = $timeplanId;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     *
     * @return CreateActivityCommand
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     *
     * @return CreateActivityCommand
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     *
     * @return CreateActivityCommand
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CreateActivityCommand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     *
     * @return CreateActivityCommand
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return array
     */
    public function getCategoryIds()
    {
        return $this->categoryIds;
    }

    /**
     * @param array $categoryIds
     *
     * @return CreateActivityCommand
     */
    public function setCategoryIds($categoryIds)
    {
        $this->categoryIds = $categoryIds;

        return $this;
    }
}
