<?php

namespace Tworzenieweb\SoloPlanner\Application\Command;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application\Command
 */
class ViewActivitiesCommand
{
    /**
     * @var string
     */
    private $view;

    /**
     * @var string
     */
    private $timeplanId;

    /**
     * @var string
     */
    private $context;

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param string $view
     *
     * @return $this
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeplanId()
    {
        return $this->timeplanId;
    }

    /**
     * @param string $timeplanId
     *
     * @return $this
     */
    public function setTimeplanId($timeplanId)
    {
        $this->timeplanId = $timeplanId;

        return $this;
    }

    /**
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param string $context
     *
     * @return $this
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }
}
