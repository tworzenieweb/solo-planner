<?php

namespace Tworzenieweb\SoloPlanner\Application\Handler;

use Broadway\CommandHandling\CommandHandler;
use Tworzenieweb\SoloPlanner\Domain\Category\CategorySpecification;
use Tworzenieweb\SoloPlanner\Domain\Category\FilterByIdsCategorySpecification;
use Tworzenieweb\SoloPlanner\Domain\CategoryRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\MetaData;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Application\Command\CreateActivityCommand;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Timeframe;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\TimeplanRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application\Handler
 */
class CreateActivityHandler extends CommandHandler
{
    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var TimeplanRepository
     */
    private $timeplanRepository;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var CategorySpecification
     */
    private $categorySpecification;

    /**
     * @param TimeplanRepository               $timeplanRepository
     * @param ActivityRepository               $activityRepository
     * @param CategoryRepository               $categoryRepository
     * @param FilterByIdsCategorySpecification $categorySpecification
     */
    public function __construct(
        TimeplanRepository $timeplanRepository,
        ActivityRepository $activityRepository,
        CategoryRepository $categoryRepository,
        FilterByIdsCategorySpecification $categorySpecification
    )
    {
        $this->timeplanRepository = $timeplanRepository;
        $this->activityRepository = $activityRepository;
        $this->categoryRepository = $categoryRepository;
        $this->categorySpecification = $categorySpecification;
    }

    /**
     * @param CreateActivityCommand $command
     *
     * @throws TimeplanNotFoundException
     */
    public function handleCreateActivityCommand(CreateActivityCommand $command)
    {
        $timeplan = $this->timeplanRepository->ofId(TimeplanId::valueOf($command->getTimeplanId()));

        if (null === $timeplan) {
            throw new TimeplanNotFoundException(sprintf('Timeplan for id %s was not found', $command->getTimeplanId()));
        }

        $timeframe = $this->buildTimeframeFromCommand($command);
        $metaData = $this->buildMetaDataFromCommand($command);
        $categories = $this->buildCategoriesFromCommand($command);

        $this->activityRepository->add(
            $timeplan->makeActivity(
                $this->activityRepository->nextIdentity(),
                $timeframe,
                $metaData,
                $categories
            )
        );
    }

    /**
     * @param CreateActivityCommand $command
     *
     * @return Timeframe
     */
    private function buildTimeframeFromCommand(CreateActivityCommand $command)
    {
        return Timeframe::fromDateAndTimes(
            new Date($command->getStartDate()),
            Time::valueOf($command->getStartTime()),
            Time::valueOf($command->getEndTime())
        );
    }

    /**
     * @param CreateActivityCommand $command
     *
     * @return MetaData
     */
    private function buildMetaDataFromCommand(CreateActivityCommand $command)
    {
        return new MetaData($command->getName(), $command->getNote());
    }

    /**
     * @param CreateActivityCommand $command
     *
     * @return CategoriesCollection
     */
    private function buildCategoriesFromCommand(CreateActivityCommand $command)
    {
        $this->categorySpecification->filterByIds($command->getCategoryIds());
        $categories = $this->categoryRepository->match($this->categorySpecification);

        return CategoriesCollection::fromArray($categories);
    }
}
