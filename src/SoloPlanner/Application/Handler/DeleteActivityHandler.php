<?php

namespace Tworzenieweb\SoloPlanner\Application\Handler;

use Broadway\CommandHandling\CommandHandler;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityCannotBeDeletedException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Domain\User\UserId;
use Tworzenieweb\SoloPlanner\Domain\User\UserNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\UserRepository;
use Tworzenieweb\SoloPlanner\Application\Command\DeleteActivityCommand;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application\Handler
 */
class DeleteActivityHandler extends CommandHandler
{
    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param ActivityRepository $activityRepository
     * @param UserRepository     $userRepository
     */
    public function __construct(ActivityRepository $activityRepository, UserRepository $userRepository)
    {
        $this->activityRepository = $activityRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param DeleteActivityCommand $deleteActivityCommand
     *
     * @throws ActivityNotFoundException
     */
    public function handleDeleteActivityCommand(DeleteActivityCommand $deleteActivityCommand)
    {
        $userId = $deleteActivityCommand->getUserId();
        $user = $this->userRepository->ofId(UserId::valueOf($userId));
        $activityId = $deleteActivityCommand->getId();
        $activity = $this->activityRepository->ofId(ActivityId::valueOf($activityId));

        if (null === $user) {
            throw new UserNotFoundException(sprintf('User for id %s was not found', $userId));
        }

        if (null === $activity) {
            throw new ActivityNotFoundException(sprintf('Activity for id %s was not found', $activityId));
        }

        if (!$user->canDelete($activity)) {
            throw new ActivityCannotBeDeletedException(
                sprintf('Activity for id %s cannot be deleted by user %s', $activityId, $userId)
            );
        }

        $this->activityRepository->remove($activity);
    }
}
