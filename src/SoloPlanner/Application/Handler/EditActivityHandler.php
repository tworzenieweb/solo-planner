<?php

namespace Tworzenieweb\SoloPlanner\Application\Handler;

use Broadway\CommandHandling\CommandHandler;
use Tworzenieweb\SoloPlanner\Domain\Category\FilterByIdsCategorySpecification;
use Tworzenieweb\SoloPlanner\Domain\CategoryRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityCannotBeEditedException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time;
use Tworzenieweb\SoloPlanner\Domain\User\UserId;
use Tworzenieweb\SoloPlanner\Domain\User\UserNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\UserRepository;
use Tworzenieweb\SoloPlanner\Application\Command\EditActivityCommand;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application\Handler
 */
class EditActivityHandler extends CommandHandler
{
    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var FilterByIdsCategorySpecification
     */
    private $categorySpecification;

    /**
     * @param ActivityRepository               $activityRepository
     * @param UserRepository                   $userRepository
     * @param CategoryRepository               $categoryRepository
     * @param FilterByIdsCategorySpecification $categorySpecification
     */
    public function __construct(
        ActivityRepository $activityRepository,
        UserRepository $userRepository,
        CategoryRepository $categoryRepository,
        FilterByIdsCategorySpecification $categorySpecification
    )
    {
        $this->activityRepository = $activityRepository;
        $this->userRepository = $userRepository;
        $this->categoryRepository = $categoryRepository;
        $this->categorySpecification = $categorySpecification;
    }

    /**
     * @param EditActivityCommand $command
     *
     * @throws ActivityCannotBeEditedException
     * @throws ActivityNotFoundException
     * @throws UserNotFoundException
     */
    public function handleEditActivityCommand(EditActivityCommand $command)
    {
        $userId = $command->getUserId();
        $user = $this->userRepository->ofId(UserId::valueOf($userId));
        $activityId = $command->getActivityId();
        $activity = $this->activityRepository->ofId(ActivityId::valueOf($activityId));

        if (null === $user) {
            throw new UserNotFoundException(sprintf('User for id %s was not found', $userId));
        }

        if (null === $activity) {
            throw new ActivityNotFoundException(sprintf('Activity for id %s was not found', $activityId));
        }

        if (!$user->canEdit($activity)) {
            throw new ActivityCannotBeEditedException(
                sprintf('Activity for id %s cannot be edited by user %s', $activityId, $userId)
            );
        }

        $this->updateActivity($command, $activity);
    }

    /**
     * @param EditActivityCommand $command
     * @param                     $activity
     */
    private function updateActivity(EditActivityCommand $command, Activity $activity)
    {
        if ($command->getName()) {
            $activity->changeName($command->getName());
        }

        if ($command->getNote()) {
            $activity->changeNote($command->getName());
        }

        if ($command->getStartAtDate()) {
            $activity->changeStartAtDate(new Date($command->getStartAtDate()));
        }

        if ($command->getStartTime()) {
            $activity->changeStartTime(Time::valueOf($command->getStartTime()));
        }

        if ($command->getEndTime()) {
            $activity->changeEndTime(Time::valueOf($command->getEndTime()));
        }

        if ($command->getCategoryIds()) {
            $this->categorySpecification->filterByIds($command->getCategoryIds());

            $categories = $this->categoryRepository->match(
                $this->categorySpecification
            );

            $activity->changeCategories(CategoriesCollection::fromArray($categories));
        }
    }
}
