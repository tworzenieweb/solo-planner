<?php

namespace Tworzenieweb\SoloPlanner\Application\Handler;

use Broadway\CommandHandling\CommandHandler;
use Tworzenieweb\SoloPlanner\Application\ViewDTO;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\DateContext;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\DateContextActivitySpecification;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\TimeplanRepository;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;
use Tworzenieweb\SoloPlanner\Application\Command\ViewActivitiesCommand;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application\Handler
 */
class ViewActivitiesHandler extends CommandHandler
{
    /**
     * @var TimeplanRepository
     */
    private $timeplanRepository;

    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var DateContextActivitySpecification
     */
    private $activitySpecification;

    /**
     * @var ViewDTO
     */
    private $viewDTO;

    /**
     * @param TimeplanRepository               $timeplanRepository
     * @param ActivityRepository               $activityRepository
     * @param DateContextActivitySpecification $activitySpecification
     * @param ViewDTO                          $viewDTO
     */
    public function __construct(
        TimeplanRepository $timeplanRepository,
        ActivityRepository $activityRepository,
        DateContextActivitySpecification $activitySpecification,
        ViewDTO $viewDTO
    )
    {
        $this->timeplanRepository = $timeplanRepository;
        $this->activityRepository = $activityRepository;
        $this->activitySpecification = $activitySpecification;
        $this->viewDTO = $viewDTO;
    }

    /**
     * @param ViewActivitiesCommand $viewActivitiesCommand
     *
     * @return array
     * @throws TimeplanNotFoundException
     */
    public function handleViewActivitiesCommand(ViewActivitiesCommand $viewActivitiesCommand)
    {
        $timeplanId = TimeplanId::valueOf($viewActivitiesCommand->getTimeplanId());
        $timeplan = $this->timeplanRepository->ofId($timeplanId);

        if (null === $timeplan) {
            throw new TimeplanNotFoundException(
                sprintf('Timeplan for id %s was not found', $viewActivitiesCommand->getTimeplanId())
            );
        }

        $this->activitySpecification->filterByDateContext(
            DateContext::fromViewAndContext($viewActivitiesCommand->getView(), $viewActivitiesCommand->getContext())
        );

        $this->viewDTO->activities = $this->activityRepository->match($this->activitySpecification);
    }
}
