<?php


namespace Tworzenieweb\SoloPlanner\Application;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application
 */
class ViewDTO
{
    /**
     * @var Activity[]
     */
    public $activities;
}