<?php

namespace Tworzenieweb\SoloPlanner\Domain;

use Tworzenieweb\SoloPlanner\Domain\Category\CategoryId;

/**
 * Class Category
 * @package Tworzenieweb\SoloPlanner\Domain
 */
class Category
{
    /**
     * @var CategoryId
     */
    private $categoryId;

    /**
     * @var string
     */
    private $name;

    /**
     * @param CategoryId $categoryId
     * @param string     $name
     */
    public function __construct(CategoryId $categoryId, $name)
    {
        $this->categoryId = $categoryId;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return CategoryId
     */
    public function getId()
    {
        return $this->categoryId;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
