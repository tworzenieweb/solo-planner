<?php

namespace Tworzenieweb\SoloPlanner\Domain\Category;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\BaseIdentity;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Category
 */
class CategoryId extends BaseIdentity
{
}
