<?php


namespace Tworzenieweb\SoloPlanner\Domain\Category;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Category
 */
class CategoryNotFoundException extends \Exception
{
}