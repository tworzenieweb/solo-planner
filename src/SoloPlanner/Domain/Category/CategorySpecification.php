<?php


namespace Tworzenieweb\SoloPlanner\Domain\Category;

use Tworzenieweb\SoloPlanner\Domain\CategoryRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Category
 */
interface CategorySpecification
{
    /**
     * @param CategoryRepository $repository
     *
     * @return mixed
     */
    public function match(CategoryRepository $repository);
}