<?php


namespace Tworzenieweb\SoloPlanner\Domain\Category;

use Tworzenieweb\SoloPlanner\Domain\Category;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Category
 */
interface FilterByIdsCategorySpecification extends CategorySpecification
{
    /**
     * @param array $ids
     *
     * @return self
     */
    public function filterByIds(array $ids);
}