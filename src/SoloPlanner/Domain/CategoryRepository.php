<?php

namespace Tworzenieweb\SoloPlanner\Domain;

use Tworzenieweb\SoloPlanner\Domain\Category\CategoryId;
use Tworzenieweb\SoloPlanner\Domain\Category\CategorySpecification;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain
 */
interface CategoryRepository
{
    /**
     * @param CategoryId $categoryId
     *
     * @return Category
     */
    public function ofId(CategoryId $categoryId);

    /**
     * @param Category $category
     *
     * @return self
     */
    public function add(Category $category);

    /**
     * @return CategoryId
     */
    public function nextIdentity();

    /**
     * @param CategorySpecification $specification
     *
     * @return Category[]
     */
    public function match(CategorySpecification $specification);
}