<?php


namespace Tworzenieweb\SoloPlanner\Domain\Event;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain
 */
class ActivityWasCreatedEvent
{
    /**
     * @var Activity
     */
    public $activity;

    /**
     * @param Activity $activity
     */
    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }
}