<?php

namespace Tworzenieweb\SoloPlanner\Domain;

use Broadway\EventSourcing\EventSourcedAggregateRoot;
use Tworzenieweb\SoloPlanner\Domain\Event\ActivityWasCreatedEvent;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityAlreadyExistsException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\MetaData;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Timeframe;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class Timeplan extends EventSourcedAggregateRoot
{
    /**
     * @var TimeplanId
     */
    private $timeplanId;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Activity[]
     */
    private $activities;

    /**
     * @param TimeplanId $timeplanId
     * @param User       $user
     */
    public function __construct(TimeplanId $timeplanId, User $user)
    {
        $this->timeplanId = $timeplanId;
        $this->user = $user;
        $this->activities = [];
    }

    /**
     * @return TimeplanId
     */
    public function getId()
    {
        return $this->timeplanId;
    }

    /**
     * @param Activity $activity
     *
     * @return self
     */
    protected function addActivity(Activity $activity)
    {
        array_push($this->activities, $activity);

        return $this;
    }

    /**
     * @param ActivityId $activityId
     * @param Timeframe $timeframe
     * @param MetaData $metaData
     * @param CategoriesCollection $categories
     *
     * @return Activity
     */
    public function makeActivity(
        ActivityId $activityId,
        Timeframe $timeframe,
        MetaData $metaData,
        CategoriesCollection $categories
    )
    {
        $activity = new Activity($activityId, $timeframe, $this, $metaData, $categories);

        if (in_array($activity, $this->activities)) {
            throw new ActivityAlreadyExistsException(
                sprintf(
                    'You have already added activity with attributes %s, %s, %s, %s',
                    (string) $activityId,
                    (string) $timeframe,
                    (string) $metaData,
                    (string) $categories
                )
            );
        }

        $this->apply(new ActivityWasCreatedEvent($activity));

        return $activity;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Activity[]
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * @return string
     */
    public function getAggregateRootId()
    {
        return $this->timeplanId->getId();
    }

    /**
     * @param ActivityWasCreatedEvent $event
     */
    public function applyActivityWasCreatedEvent(ActivityWasCreatedEvent $event)
    {
        $this->addActivity($event->activity);
    }
}
