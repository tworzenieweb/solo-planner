<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan;

use Broadway\EventSourcing\EventSourcedAggregateRoot;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;
use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\CategoriesCollection;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\MetaData;
use Tworzenieweb\SoloPlanner\Domain\User;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class Activity
{
    /**
     * @var ActivityId
     */
    private $activityId;

    /**
     * @var Timeplan
     */
    private $timeplan;

    /**
     * @var Timeframe
     */
    private $timeframe;

    /**
     * @var MetaData
     */
    private $metaData;

    /**
     * @var CategoriesCollection
     */
    private $categories;

    /**
     * @param ActivityId           $activityId
     * @param Timeframe            $timeframe
     * @param Timeplan             $timeplan
     * @param MetaData             $metaData
     * @param CategoriesCollection $categories
     */
    public function __construct(
        ActivityId $activityId,
        Timeframe $timeframe,
        Timeplan $timeplan,
        MetaData $metaData,
        CategoriesCollection $categories
    )
    {
        $this->activityId = $activityId;
        $this->timeframe = $timeframe;
        $this->timeplan = $timeplan;
        $this->metaData = $metaData;
        $this->categories = $categories;
    }

    /**
     * @return ActivityId
     */
    public function getId()
    {
        return $this->activityId;
    }

    /**
     * @return Date
     */
    public function getStartAtDate()
    {
        return $this->timeframe->getDate();
    }

    /**
     * @return Time
     */
    public function getStartTime()
    {
        return $this->timeframe->getStartTime();
    }

    /**
     * @return Time
     */
    public function getEndTime()
    {
        return $this->timeframe->getEndTime();
    }

    /**
     * @return CategoriesCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->metaData->getName();
    }

    /**
     * @return null|string
     */
    public function getNote()
    {
        return $this->metaData->getNote();
    }

    /**
     * @return Timeplan
     */
    public function getTimeplan()
    {
        return $this->timeplan;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->timeplan->getUser();
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function changeName($name)
    {
        $this->metaData = $this->metaData->changeName($name);

        return $this;
    }

    /**
     * @param string $note
     *
     * @return $this
     */
    public function changeNote($note)
    {
        $this->metaData = $this->metaData->changeNote($note);

        return $this;
    }

    /**
     * @param Date $startAtDate
     *
     * @return $this
     */
    public function changeStartAtDate(Date $startAtDate)
    {
        $this->timeframe = $this->timeframe->changeStartAtDate($startAtDate);

        return $this;
    }

    /**
     * @param Time $startTime
     *
     * @return $this
     */
    public function changeStartTime(Time $startTime)
    {
        $this->timeframe = $this->timeframe->changeStartTime($startTime);

        return $this;
    }

    /**
     * @param Time $endTime
     *
     * @return $this
     */
    public function changeEndTime(Time $endTime)
    {
        $this->timeframe = $this->timeframe->changeEndTime($endTime);

        return $this;
    }

    /**
     * @param CategoriesCollection $categoriesCollection
     *
     * @return $this
     */
    public function changeCategories(CategoriesCollection $categoriesCollection)
    {
        $this->categories = $categoriesCollection;

        return $this;
    }
}
