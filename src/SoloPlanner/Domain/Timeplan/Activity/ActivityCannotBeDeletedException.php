<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use RuntimeException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application\Handler
 */
class ActivityCannotBeDeletedException extends RuntimeException
{
}