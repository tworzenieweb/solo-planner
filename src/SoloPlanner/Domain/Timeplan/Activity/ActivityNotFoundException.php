<?php


namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class ActivityNotFoundException extends \Exception
{
}