<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
interface ActivitySpecification
{
    /**
     * @param ActivityRepository $repository
     *
     * @return mixed
     */
    public function match(ActivityRepository $repository);
}