<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use Assert\Assertion;
use Assert\InvalidArgumentException;
use Tworzenieweb\SoloPlanner\Domain\Category;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class CategoriesCollection
{
    /**
     * @var Category[]
     */
    private $categories;

    /**
     * @param array $categories
     *
     * @throws EmptyCategoriesCollectionException
     * @throws InvalidCategoriesCollectionException
     */
    private function __construct(array $categories)
    {
        if (!count($categories)) {
            throw new EmptyCategoriesCollectionException(sprintf('Collection needs to contain at least one Category'));
        }

        try {
            Assertion::allIsInstanceOf(
                $categories,
                Category::class,
                'All elements should be of type Category'
            );
        } catch (InvalidArgumentException $invalidArgumentException) {
            throw new InvalidCategoriesCollectionException($invalidArgumentException);
        }

        $this->categories = $categories;
    }

    /**
     * @param array $categories
     *
     * @return CategoriesCollection
     */
    public static function fromArray(array $categories)
    {
        $categoriesCollection = new self($categories);

        return $categoriesCollection;
    }

    /**
     * @param Category $category
     *
     * @return CategoriesCollection
     */
    public function add(Category $category)
    {
        $categories = $this->categories;
        array_push($categories, $category);

        return new self($categories);
    }

    /**
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return implode(',', $this->categories);
    }
}
