<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use Assert\Assertion;
use Exception;
use InvalidArgumentException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
abstract class DateContext
{
    const VIEW_DAY = 'day';
    const VIEW_MONTH = 'month';
    const VIEW_YEAR = 'year';
    const VIEW_WEEK = 'week';
    const INVALID_VIEW_CONTEXT = 'Invalid context value for week. It should match format year/W[week_number] for example 2016/W01';
    const INVALID_MONTH_CONTEXT = 'Invalid month context. It should be mm/YYYY';
    const INVALID_DAY_CONTEXT = 'Invalid day context - it should be YYYY-mm-dd';

    /**
     * @var Date
     */
    protected $context;

    /**
     * @var Date
     */
    protected $startDate;

    /**
     * @var Date
     */
    protected $endDate;

    /**
     * @param string $view
     * @param string $context
     *
     * @return DateContext
     */
    public static function fromViewAndContext($view, $context)
    {
        try {
            switch ($view) {
                case self::VIEW_DAY:
                    Assertion::regex($context, '/^\d{4}\-\d{2}\-\d{2}$/', self::INVALID_DAY_CONTEXT);
                    $date = new Date($context);

                    return new DayDateContext($date);
                case self::VIEW_WEEK:
                    Assertion::regex($context, '/^\d{4}\/W\d{2}$/', self::INVALID_VIEW_CONTEXT);
                    list($year, $week) = explode('/W', $context);
                    $date = new Date();
                    $date = $date->setISODate($year, $week, 1);

                    return new WeekDateContext($date);
                case self::VIEW_MONTH:
                    Assertion::regex($context, '/^\d{2}\/\d{4}$/', self::INVALID_MONTH_CONTEXT);
                    $date = new Date(sprintf('01/%s', $context));

                    return new MonthDateContext($date);
                case self::VIEW_YEAR:
                    Assertion::regex($context, '/^\d{4}$/', 'Invalid year context. It should be YYYY');
                    $date = new Date(sprintf('01/01/%d', $context));

                    return new YearDateContext($date);
            }

            throw new InvalidArgumentException(sprintf('Unsupported view %s provided', $view));

        } catch (Exception $exception) {
            throw new InvalidDateContextException($exception->getMessage());
        }

    }

    /**
     * @return Date
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return Date
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param Date $target
     *
     * @return bool
     */
    public function has(Date $target)
    {
        return $target >= $this->startDate && $target <= $this->endDate;
    }
}
