<?php


namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
interface DateContextActivitySpecification extends ActivitySpecification
{
    /**
     * @param DateContext $context
     *
     * @return self
     */
    public function filterByDateContext(DateContext $context);
}