<?php


namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class DayDateContext extends DateContext
{
    /**
     * @param Date $context
     */
    protected function __construct(Date $context)
    {
        $this->context = $context;
        $this->startDate = $this->context->setTime(0, 0, 0);
        $this->endDate = $this->context->setTime(23, 59, 59);
    }
}