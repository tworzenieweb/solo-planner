<?php


namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use Exception;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class InvalidCategoriesCollectionException extends Exception
{

}