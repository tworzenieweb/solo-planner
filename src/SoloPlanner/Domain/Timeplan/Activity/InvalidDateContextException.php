<?php


namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use InvalidArgumentException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class InvalidDateContextException extends InvalidArgumentException
{
}