<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class MetaData
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $note;

    /**
     *
     * @param string $name
     * @param string $note
     */
    public function __construct($name, $note = null)
    {
        $this->name = $name;
        $this->note = $note;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $name
     *
     * @return MetaData
     */
    public function changeName($name)
    {
        return new self($name, $this->note);
    }

    /**
     * @param string $note
     *
     * @return MetaData
     */
    public function changeNote($note)
    {
        return new self($this->name, $note);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name . "\n" . $this->note;
    }
}
