<?php


namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class WeekDateContext extends DateContext
{

    /**
     * @param Date $context
     */
    protected function __construct(Date $context)
    {
        $this->context = $context;
        $this->startDate = $this->context->modify('Monday');
        $this->endDate = $this->context->modify('Sunday');
    }
}