<?php


namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity
 */
class YearDateContext extends DateContext
{
    /**
     * @param Date $context
     */
    protected function __construct(Date $context)
    {
        $this->context = $context;
        $this->startDate = $this->context->modify('first day of this year 00:00');
        $this->endDate = $this->context->modify('last day of december this year 23:59');
    }
}