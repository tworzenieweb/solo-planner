<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivitySpecification;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
interface ActivityRepository
{
    /**
     * @param ActivityId $activityId
     *
     * @return Activity
     */
    public function ofId(ActivityId $activityId);

    /**
     * @param string $name
     *
     * @return Activity[]
     */
    public function ofName($name);

    /**
     * @param Activity $activity
     * @return self
     */
    public function add(Activity $activity);

    /**
     * @return ActivityId
     */
    public function nextIdentity();

    /**
     * @param ActivitySpecification $specification
     *
     * @return Activity[]
     */
    public function match(ActivitySpecification $specification);

    /**
     * @param Activity $activity
     *
     * @return self
     */
    public function remove(Activity $activity);
}
