<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan;

use Rhumsaa\Uuid\Uuid;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class BaseIdentity
{
    /**
     * @var string
     */
    private $id;

    /**
     *
     * @param string $id
     */
    protected function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return static
     */
    public static function getNextId()
    {
        $uuid = Uuid::uuid4();

        return new static($uuid->toString());
    }

    /**
     * @param string $uuid
     *
     * @return static
     */
    public static function valueOf($uuid)
    {
        return new static($uuid);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }
}