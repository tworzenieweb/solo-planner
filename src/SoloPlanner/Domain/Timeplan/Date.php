<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan;

use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date\InvalidArgumentException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class Date extends DateTimeImmutable
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @param string       $time
     * @param DateTimeZone $timezone
     */
    public function __construct($time = 'now', $timezone = null)
    {
        try {
            parent::__construct($time, $timezone);
        } catch (Exception $exception) {
            throw new InvalidArgumentException(sprintf('Provided invalid date %s', $time));
        }
    }

    /**
     * @param int $year
     * @param int $week
     * @param int $day
     *
     * @return Date
     */
    public function setISODate($year, $week, $day = 1)
    {
        $object = parent::setISODate($year, $week, $day);

        return new self($object->format('Y-m-d 00:00:00'));
    }

    /**
     * @param int $hour
     * @param int $minute
     * @param int $second
     *
     * @return Date
     */
    public function setTime($hour, $minute, $second = 0)
    {
        $object = parent::setTime($hour, $minute, $second);

        return new self($object->format(self::DATE_FORMAT));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format(self::DATE_FORMAT);
    }
}
