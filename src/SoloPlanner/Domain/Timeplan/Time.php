<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan;

use Assert\Assertion;
use Assert\InvalidArgumentException as BaseInvalidArgumentException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Time\InvalidArgumentException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class Time
{
    /**
     * @var string
     */
    private $time;

    /**
     * @param $time
     * @throws InvalidArgumentException
     */
    private function __construct($time)
    {
        try {
            Assertion::date($time, 'H:i', 'time needs to be a valid time in format H:i');
            $this->time = $time;
        } catch (BaseInvalidArgumentException $invalidArgumentException) {
            throw new InvalidArgumentException('time needs to be a valid time in format H:i');
        }
    }


    /**
     * @param string $time
     *
     * @return Time
     */
    public static function valueOf($time)
    {
        return new self($time);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->time;
    }
}
