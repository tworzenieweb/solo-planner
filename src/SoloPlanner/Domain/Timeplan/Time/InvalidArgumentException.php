<?php


namespace Tworzenieweb\SoloPlanner\Domain\Timeplan\Time;

use InvalidArgumentException as BaseException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class InvalidArgumentException extends BaseException
{
}