<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Date;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain\Timeplan
 */
class Timeframe
{
    /**
     * @var Date
     */
    private $date;

    /**
     * @var Time
     */
    private $startTime;

    /**
     * @var Time
     */
    private $endTime;

    /**
     * @param Date $date
     * @param Time $startTime
     * @param Time $endTime
     */
    private function __construct(Date $date, Time $startTime, Time $endTime)
    {
        $this->date = $date;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
    }

    /**
     * @param Date $date
     * @param Time $startTime
     * @param Time $endTime
     *
     * @return Timeframe
     */
    public static function fromDateAndTimes(Date $date, Time $startTime, Time $endTime)
    {
        $timeframe = new self($date, $startTime, $endTime);

        return $timeframe;
    }

    /**
     * @return Date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return Time
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @return Time
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param Date $date
     *
     * @return Timeframe
     */
    public function changeStartAtDate(Date $date)
    {
        return self::fromDateAndTimes($date, $this->startTime, $this->endTime);
    }

    /**
     * @param Time $startTime
     *
     * @return Timeframe
     */
    public function changeStartTime(Time $startTime)
    {
        return self::fromDateAndTimes($this->date, $startTime, $this->endTime);
    }

    /**
     * @param Time $endTime
     *
     * @return Timeframe
     */
    public function changeEndTime(Time $endTime)
    {
        return self::fromDateAndTimes($this->date, $this->startTime, $endTime);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return sprintf('%s: %s - %s', $this->date, $this->startTime, $this->endTime);
    }
}
