<?php

namespace Tworzenieweb\SoloPlanner\Domain\Timeplan;

use Exception;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Infrastructure\Timeplan
 */
class TimeplanAlreadyExists extends Exception
{
}