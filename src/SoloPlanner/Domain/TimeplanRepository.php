<?php

namespace Tworzenieweb\SoloPlanner\Domain;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain
 */
interface TimeplanRepository
{
    /**
     * @param TimeplanId $timeplanId
     *
     * @return Timeplan
     */
    public function ofId(TimeplanId $timeplanId);

    /**
     * @param Timeplan $timeplan
     * @return self
     */
    public function add(Timeplan $timeplan);

    /**
     * @return TimeplanId
     */
    public function nextIdentity();
}