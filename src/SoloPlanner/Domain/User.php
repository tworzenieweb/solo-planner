<?php

namespace Tworzenieweb\SoloPlanner\Domain;

use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;
use Tworzenieweb\SoloPlanner\Domain\User\UserId;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain
 */
class User
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var array
     */
    private $timeplans;

    /**
     * @param UserId $userId
     */
    public function __construct(UserId $userId)
    {
        $this->id = $userId;
        $this->timeplans = [];
    }

    /**
     * @return UserId
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Timeplan
     */
    public function createTimeplan()
    {
        $timeplan = new Timeplan(TimeplanId::getNextId(), $this);
        array_push($this->timeplans, $timeplan);

        return $timeplan;
    }

    /**
     * @param Activity $activity
     *
     * @return bool
     */
    public function canDelete(Activity $activity)
    {
        return $activity->getUser() === $this;
    }

    /**
     * @param Activity $activity
     *
     * @return bool
     */
    public function canEdit(Activity $activity)
    {
        return $activity->getUser() === $this;
    }
}
