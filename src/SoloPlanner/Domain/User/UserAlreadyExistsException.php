<?php


namespace Tworzenieweb\SoloPlanner\Domain\User;

use RuntimeException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Infrastructure\User
 */
class UserAlreadyExistsException extends RuntimeException
{
}