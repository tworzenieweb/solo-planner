<?php

namespace Tworzenieweb\SoloPlanner\Domain\User;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\BaseIdentity;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain
 */
class UserId extends BaseIdentity
{
}