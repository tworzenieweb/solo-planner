<?php

namespace Tworzenieweb\SoloPlanner\Domain\User;

use RuntimeException;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Application\Handler
 */
class UserNotFoundException extends RuntimeException
{
}