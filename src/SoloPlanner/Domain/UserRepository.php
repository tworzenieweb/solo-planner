<?php


namespace Tworzenieweb\SoloPlanner\Domain;

use Tworzenieweb\SoloPlanner\Domain\User\UserId;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Domain
 */
interface UserRepository
{
    /**
     * @param UserId $userId
     *
     * @return User
     */
    public function ofId(UserId $userId);

    /**
     * @param User $user
     *
     * @return self
     */
    public function add(User $user);

    /**
     * @return UserId
     */
    public function nextIdentity();
}