<?php

namespace Tworzenieweb\SoloPlanner\Infrastructure\Activity;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivityNotFoundException;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\ActivitySpecification;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityId;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Infrastructure\Activity
 */
class InMemoryActivityRepository implements ActivityRepository
{
    /**
     * @var Activity[]
     */
    private $activities;


    /**
     */
    public function __construct()
    {
        $this->activities = [];
    }


    /**
     * @param ActivityId $activityId
     *
     * @return Activity
     */
    public function ofId(ActivityId $activityId)
    {
        if (!array_key_exists($activityId->getId(), $this->activities)) {
            return null;
        }

        return $this->activities[$activityId->getId()];
    }

    /**
     * @param string $name
     *
     * @return Activity[]
     */
    public function ofName($name)
    {
        $results = [];

        foreach ($this->activities as $activity) {
            if (strstr($activity->getName(), $name) !== false) {
                array_push($results, $activity);
            }
        }

        return $results;
    }

    /**
     * @param Activity $activity
     *
     * @return self
     */
    public function add(Activity $activity)
    {
        $this->activities[$activity->getId()->getId()] = $activity;

        return $this;
    }

    /**
     * @return Activity[]
     */
    public function getAll()
    {
        return $this->activities;
    }

    /**
     * @return ActivityId
     */
    public function nextIdentity()
    {
        return ActivityId::getNextId();
    }

    /**
     * @param ActivitySpecification $specification
     *
     * @return Activity[]
     */
    public function match(ActivitySpecification $specification)
    {
        return $specification->match($this);
    }

    /**
     * @return void
     */
    public function purge()
    {
        $this->activities = [];
    }

    /**
     * @param Activity $activity
     *
     * @return self
     */
    public function remove(Activity $activity)
    {
        $id = $activity->getId()->getId();

        if (array_key_exists($id, $this->activities)) {
            unset($this->activities[$id]);
        }

        return $this;
    }
}
