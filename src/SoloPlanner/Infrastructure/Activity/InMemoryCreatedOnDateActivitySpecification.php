<?php

namespace Tworzenieweb\SoloPlanner\Infrastructure\Activity;

use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\DateContext;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\Activity\DateContextActivitySpecification;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\ActivityRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Infrastructure\Activity
 */
class InMemoryCreatedOnDateActivitySpecification implements DateContextActivitySpecification
{
    /**
     * @var DateContext
     */
    private $dateContext;

    /**
     * @param DateContext $dateContext
     *
     * @return $this
     */
    public function filterByDateContext(DateContext $dateContext)
    {
        $this->dateContext = $dateContext;

        return $this;
    }


    /**
     * @param ActivityRepository|InMemoryActivityRepository $repository
     *
     * @return Activity[]
     */
    public function match(ActivityRepository $repository)
    {
        if (null === $this->dateContext) {
            throw new \InvalidArgumentException(sprintf('You need to set date for filtering'));
        }

        $filtered = array();

        foreach ($repository->getAll() as $activity) {
            if ($this->dateContext->has($activity->getStartAtDate())) {
                array_push($filtered, $activity);
            }
        }

        return $filtered;
    }
}
