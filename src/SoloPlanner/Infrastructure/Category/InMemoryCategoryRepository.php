<?php

namespace Tworzenieweb\SoloPlanner\Infrastructure\Category;

use Tworzenieweb\SoloPlanner\Domain\Category;
use Tworzenieweb\SoloPlanner\Domain\Category\CategoryId;
use Tworzenieweb\SoloPlanner\Domain\Category\CategorySpecification;
use Tworzenieweb\SoloPlanner\Domain\CategoryRepository;

/**
 * @author Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Infrastructure\Category
 */
class InMemoryCategoryRepository implements CategoryRepository
{
    /**
     * @var Category[]
     */
    private $categories;

    /**
     * InMemoryCategoryRepository constructor.
     */
    public function __construct()
    {
        $this->categories = [];
    }


    /**
     * @param CategoryId $categoryId
     *
     * @return Category|null
     */
    public function ofId(CategoryId $categoryId)
    {
        if (!array_key_exists($categoryId->getId(), $this->categories)) {
            return null;
        }

        return $this->categories[$categoryId->getId()];
    }

    /**
     * @param Category $category
     *
     * @return self
     */
    public function add(Category $category)
    {
        $this->categories[$category->getId()->getId()] = $category;

        return $this;
    }

    /**
     * @return CategoryId
     */
    public function nextIdentity()
    {
        return CategoryId::getNextId();
    }

    /**
     * @param CategorySpecification $specification
     *
     * @return mixed
     */
    public function match(CategorySpecification $specification)
    {
        return $specification->match($this);
    }

    /**
     * @return Category[]
     */
    public function getAll()
    {
        return $this->categories;
    }
}
