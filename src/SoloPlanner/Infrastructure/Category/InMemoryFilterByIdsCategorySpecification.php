<?php

namespace Tworzenieweb\SoloPlanner\Infrastructure\Category;

use Tworzenieweb\SoloPlanner\Domain\Category;
use Tworzenieweb\SoloPlanner\Domain\Category\FilterByIdsCategorySpecification;
use Tworzenieweb\SoloPlanner\Domain\CategoryRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Infrastructure\Category
 */
class InMemoryFilterByIdsCategorySpecification implements FilterByIdsCategorySpecification
{
    /**
     * @var array
     */
    private $ids;

    /**
     * @param CategoryRepository|InMemoryCategoryRepository $repository
     *
     * @return Category[]
     */
    public function match(CategoryRepository $repository)
    {
        $filtered = array();

        foreach ($repository->getAll() as $category) {
            $id = $category->getId()->getId();

            if (in_array($id, $this->ids)) {
                array_push($filtered, $category);
            }
        }

        return $filtered;
    }

    /**
     * @param array $ids
     *
     * @return self
     */
    public function filterByIds(array $ids)
    {
        $this->ids = $ids;

        return $this;
    }
}
