<?php

namespace Tworzenieweb\SoloPlanner\Infrastructure\Timeplan;

use Tworzenieweb\SoloPlanner\Domain\Timeplan;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanAlreadyExists;
use Tworzenieweb\SoloPlanner\Domain\Timeplan\TimeplanId;
use Tworzenieweb\SoloPlanner\Domain\TimeplanRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Infrastructure\Timeplan
 */
class InMemoryTimeplanRepository implements TimeplanRepository
{
    /**
     * @var Timeplan[]
     */
    private $timeplans;

    /**
     */
    public function __construct()
    {
        $this->timeplans = [];
    }


    /**
     * @param TimeplanId $timeplanId
     *
     * @return Timeplan|null
     */
    public function ofId(TimeplanId $timeplanId)
    {
        $id = $timeplanId->getId();

        if (!array_key_exists($id, $this->timeplans)) {
            return null;
        }

        return $this->timeplans[$id];
    }

    /**
     * @param Timeplan $timeplan
     *
     * @return static
     * @throws TimeplanAlreadyExists
     */
    public function add(Timeplan $timeplan)
    {
        $id = $timeplan->getId()->getId();

        if (array_key_exists($id, $this->timeplans)) {
            throw new TimeplanAlreadyExists(sprintf('Timeplan for id %s already exists', $id));
        }

        $this->timeplans[$id] = $timeplan;

        return $this;
    }

    /**
     * @return TimeplanId
     */
    public function nextIdentity()
    {
        return TimeplanId::getNextId();
    }

    /**
     * @return Timeplan[]
     */
    public function getAll()
    {
        return $this->timeplans;
    }
}
