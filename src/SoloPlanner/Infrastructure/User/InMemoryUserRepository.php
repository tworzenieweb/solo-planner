<?php

namespace Tworzenieweb\SoloPlanner\Infrastructure\User;

use Tworzenieweb\SoloPlanner\Domain\User;
use Tworzenieweb\SoloPlanner\Domain\User\UserAlreadyExistsException;
use Tworzenieweb\SoloPlanner\Domain\User\UserId;
use Tworzenieweb\SoloPlanner\Domain\UserRepository;

/**
 * @author  Luke Adamczewski <tworzenieweb@gmail.com>
 * @package Tworzenieweb\SoloPlanner\Infrastructure\User
 */
class InMemoryUserRepository implements UserRepository
{
    /**
     * @var User[]
     */
    private $users;

    /**
     */
    public function __construct()
    {
        $this->users = [];
    }

    /**
     * @param UserId $userId
     *
     * @return User|null
     */
    public function ofId(UserId $userId)
    {
        $id = $userId->getId();

        if (!array_key_exists($id, $this->users)) {
            return null;
        }

        return $this->users[$id];
    }

    /**
     * @param User $user
     *
     * @return static
     * @throws UserAlreadyExistsException
     */
    public function add(User $user)
    {
        $id = $user->getId()->getId();

        if (array_key_exists($id, $this->users)) {
            throw new UserAlreadyExistsException(sprintf('User for id %s already exists', $id));
        }

        $this->users[$id] = $user;

        return $this;
    }

    /**
     * @return UserId
     */
    public function nextIdentity()
    {
        return UserId::getNextId();
    }

    /**
     * @return User[]
     */
    public function getAll()
    {
        return $this->users;
    }
}
